**Team Win Recovery Project (TWRP)**

You can find a compiling guide [here](http://forum.xda-developers.com/showthread.php?t=1943625 "Guide").

## Current build instructions:

    #####################################################
    ### Instructions to build TWRP from a "minimal" LineageOS manifest
    ## Following
    https://www.youtube.com/playlist?list=PLRJ9-cX1yE1kzxSkdZSG_h_f-GDNmSmAl

    ### Initially fetch essential build tools:
    # sudo apt-get install openjdk-8-jdk git-core gnupg flex bison gperf build-
    essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386
    lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev libgl1-mesa-dev
    libxml2-utils xsltproc unzip schedtool


    ## Steps from https://github.com/minimal-manifest-twrp/
    platform_manifest_twrp_lineageos :

    ## unfortunately they only have a stripped down lineageos manifest for 14.1:
    repo init --depth=1 -u \
    git://github.com/minimal-manifest-twrp/platform_manifest_twrp_lineageos.git  -b twrp-14.1

    repo sync

    ## Then I currently apply my two patches:

    for commit in \
         2deb078fb269cf6a0b36fcf6770ea04fb5d0a390 \
        a0ba7582739d671c0bcf6d5f9d5b7fc81d185a13
    do
    curl https://gitlab.e.foundation/e/tools/etwrp/commit/"$commit".diff | patch -p1 -d bootable/recovery

    done


    ## Ok, now select target:
    # - Xiaomi Mi5s (capricorn)    LineageOS/android_device_xiaomi_capricorn
    # - LeEco Le2 (s2)                       LineageOS/android_device_leeco_s2
    # - Essential PH1 (mata)          LineageOS/android_device_essential_mata

    ## And build:
    source build/envsetup.sh
    lunch lineage_capricorn-eng
    ## A bit later the build stops, because the vendor files are missing
    ## but at this point the file .repo/local_manifests/roomservice.xml has been
    ## created. We have to add the correct repo, e.g. by appending a line like
    ## this to the manifest:

    sed -i '/<\/manifest>/i \
      <project name="TheMuppets/proprietary_vendor_leeco" path="s2"
      remote="github" />'   .repo/local_manifests/roomservice.xml

    ## Then sync and build again
    repo sync
    lunch lineage_capricorn-eng
    #####################################################

    Still quite a bit shaky.. maybe I should just start building full LineageOS,
    not this stripped down manifest. My current approach is not really ideal
    for eelo-0.2, because the guys that created this "minimal manifest" have
    stopped updating after cm-14.1. And there also has to be a better way to
    adjust the local manifest prior to build, instead of first failing the
    build.

    